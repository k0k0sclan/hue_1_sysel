/**
*   ERROR CODES:
*   1: Zu wenige Argumente eingegeben
*   2: Eingabefile konnte nicht geφffnet werden
*   3: Zu viele Argumente
*   4: Speicher konnte nicht angefordert werden
**/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <ctype.h>
#include <Windows.h>
#include <time.h>

#define bigger(x1,x2) (x1)<(x2)?-1: ((x1)>(x2)?1:0)
#define lesser(x1,x2) (x1)>(x2)?-1: ((x1)<(x2)?1:0)

typedef struct{
char buchNummer[11];    // Feld von 10 char und binδre 0
char *titel;            // dynamisches Textfeld geeigneter Grφίe
int bestand;            // Lagerbestand
double preis;           // Preis (in Euro)
}buch_t;


void freeData(buch_t *buecher,size_t buchAnzahl);
buch_t *fuegeBuecherFeldHinzu(char *pfad, buch_t *feld, size_t *n, size_t *maximal);
int gueltigeBuchNummer(char *isbn);
int erstelleNeuesBuch(char *str,buch_t *buecher,int buchAnzahl);
buch_t *sucheBuch(buch_t *buecher,int buchAnzahl,char *suchText,int nummerOderTitel);
char menu();
void printHeader();
void listeBuchbestand(buch_t *buecher,int anzahl);
int getMaxTitel(buch_t *buecher,int anz);
void toCSVText(buch_t *buch,char *csvText);
int speichereBuecher(char *pfad,buch_t *buecher,int anzahl);
int buchZugang(buch_t *buecher, int buchAnzahl);
void listeBuchbestandGeordnet(buch_t *buecher,int buchAnzahl,int ordnungNach);
int getSortOrder();
int buchEntfernen(buch_t *buecher,int buchListe,char* buchnummer);
int buchEntnahme(buch_t *buecher,int anzahl);
buch_t *getBuchDialog(buch_t *buecher, int anzahl);
void aendereTitel(buch_t *buchptr);
void buecherFinden(buch_t *buecher,int buchAnzahl);
void buecherSuchen(buch_t *buecher,int buchAnzahl);
void buecherFiltern(buch_t *buecher,int buchAnzahl);
double lagerWert(buch_t *buecher,int buchAnzahl);
void titelFilter(buch_t *buecher,int buchAnzahl,char *suchtext);
void nummerFilter(buch_t *buecher,int buchAnzahl,char *suchtext);
void preisFilter(buch_t *buecher,int buchAnzahl,double preis,int mehrOderWeniger);
void bestellListe(buch_t *buecher,int buchAnzahl);

int isbnCmp(const void *v1,const void *v2);
int titelCmp(const void *v1,const void *v2);
int bestandCmp(const void *v1,const void *v2);
int preisCmp(const void *v1,const void *v2);
int isbnDCmp(const void *v1,const void *v2);
int titelDCmp(const void *v1,const void *v2);
int bestandDCmp(const void *v1,const void *v2);
int preisDCmp(const void *v1,const void *v2);


int main(int argc, char *argv[])
{

    size_t buchAnzahl=0,aktFeldgroesse=10;
    buch_t *buecher,*buchptr;
    char ein;
    char pfad[100],isbn[20];

    if(argc==1){
        printf("Fehler! Es wurde keine Eingabedatei angegeben!");
        return 1;
    }
    if(argc>2){
        printf("Fehler! Es wurden zu viele Parameter angegeben!");
        return 3;
    }

    buecher = calloc(sizeof(buch_t),aktFeldgroesse);
    if(!buecher){
        printf("Fehler! Es konnte kein Speicher fuer die Buecherliste angefordert werden!");
        return 4;
    }


    printf("Buecherliste wird geladen...");
    buecher = fuegeBuecherFeldHinzu(argv[1],buecher,&buchAnzahl,&aktFeldgroesse);

    if(!buecher)
      return 2;

    do{
    fflush(stdin);
    ein = menu();

    switch(ein) {

      case 'a': listeBuchbestand(buecher,buchAnzahl); break;  //Ausgeben der Bücherliste

      case 's': listeBuchbestandGeordnet(buecher,buchAnzahl,getSortOrder()); break;  //Sortiertes ausgeben der Bücherliste

      case 'f': buecherFinden(buecher,buchAnzahl); break;

      case 'o':         //Oeffnen von zus. Datei

        printHeader();
        printf("Geben Sie den Dateinamen zum Oeffnen ein --> ");
        scanf("%s",pfad);
        buecher = fuegeBuecherFeldHinzu(pfad,buecher,&buchAnzahl,&aktFeldgroesse);
        break;

      case 'h':
        buchAnzahl += buchZugang(buecher,buchAnzahl);
        if(buchAnzahl > 0.8*(double)(aktFeldgroesse)){
          aktFeldgroesse += 20;
          buecher = realloc(buecher,(aktFeldgroesse)*sizeof(buch_t));
        }
        break;

      case 'e': buchAnzahl -= buchEntnahme(buecher,buchAnzahl); break;
      case 't':
        printHeader();
        printf("Geben Sie die ISBN des Buches ein --> ");
        gets(isbn);
        buchptr = sucheBuch(buecher,buchAnzahl,isbn,0);
        if(!buchptr){
          printHeader();
          printf("Die ISBN wurde nicht gefunden.\nTaste druecken...");
          getch();
          break;
        }
        aendereTitel(buchptr);
        break;
      case 'd':
        buchptr = getBuchDialog(buecher,buchAnzahl);
        if(buchptr)
          buchAnzahl -= buchEntfernen(buecher,buchAnzahl,buchptr->buchNummer);
        break;

      case 'b': bestellListe(buecher,buchAnzahl); break;

      case 'w':
        printHeader();
        printf("Der gesamte Lagerwert betraegt %lg EUR\nTaste druecken...",lagerWert(buecher,buchAnzahl));
        getch();
        break;

      case 'x':     //Exportieren in Datei

        printHeader();
        printf("Geben Sie den Dateinamen zum Speichern ein --> ");
        scanf("%s",pfad);
        speichereBuecher(pfad,buecher,buchAnzahl);
        break;

      case 'z': //Beenden des Programms
        do{

          printHeader();
          printf("Sind Sie sich sicher, dass Sie beenden wollen [J/n]: ");
          fflush(stdin);
          ein = getch();
        }while(!strchr("JNjn",ein));

          if(ein=='j'||ein=='J')
            ein='z';
          else
            ein=' ';

          break;

      default: break;   //Sollte aufgrund von überprüfung in menu() nicht auftreten
    }

  }while(ein!='z');

    system("cls");
    freeData(buecher,buchAnzahl);
    return 0;
}



void freeData(buch_t *buecher,size_t buchAnzahl){

    int i;

    for(i=0;i<buchAnzahl;i++)
        free(buecher[i].titel);

    free(buecher);

}

buch_t *fuegeBuecherFeldHinzu(char *pfad, buch_t *buecher, size_t *buchAnzahl, size_t *maximal){

  char str[1000]="";

  FILE *fp = fopen(pfad,"r");

  if(!fp){
      printf("Fehler! Die Eingabedatei konnte nicht geoeffnet werden!\nTaste druecken...");
      getch();
      return NULL;
  }

  fgets(str,1000,fp);     //Liest die Überschriften

  while(fgets(str,1000,fp)){

      if(str[strlen(str)-1]=='\n')
        str[strlen(str)-1]='\0';

      *buchAnzahl += erstelleNeuesBuch(str,buecher,*buchAnzahl);

      //Array vergrößern wenns zu klein ist
      if(*buchAnzahl > 0.8*(double)(*maximal)){
        *maximal += 20;
        buecher = realloc(buecher,(*maximal)*sizeof(buch_t));
      }
  }

  return buecher;
}


int gueltigeBuchNummer(char *isbn){

  int i;

  if(strlen(isbn)!=10)
    return 0;

  for(i=0;i<3;i++){
    if(!isalpha(isbn[i]))
      return 0;
  }
  for(;i<7;i++){
    if(!isdigit(isbn[i]))
      return 0;
  }
  return 1;
}


int erstelleNeuesBuch(char *str,buch_t *buecher,int buchAnzahl){

  char* cp=NULL;        //Pointer für strtok()
  char isbn[11]="";
  char* titel;
  int bestand;
  double preis;
  int test;           //für sscanf
  char c;             //auch für sscanf
  int i,sum=0;          //für werteüberprüfung

  //Auf richtige Anzahl an Spalten überprüfen
  for(i=0;i<strlen(str);i++){
      if(str[i]==';')
        sum++;
  }

  if(sum!=3)
    return 0;

  //ISBN
  cp = strtok(str,";");
  if(cp==NULL)
    return 0;
  if(!gueltigeBuchNummer(cp))
    return 0;
  strcpy(isbn,cp);

  //Titel
  cp = strtok(NULL,";");
  if(cp==NULL)
    return 0;
  titel = calloc(strlen(cp)+1,sizeof(char));
  strcpy(titel,cp);

  //Bestand
  cp = strtok(NULL,";");
  if(cp==NULL)
    return 0;
  test = sscanf(cp,"%d%c",&bestand,&c);
  if(test!=1||c!='\0'||bestand<=0)
    return 0;

  //Preis
  cp = strtok(NULL,";");
  if(cp==NULL)
    return 0;
  test = sscanf(cp,"%lf%c",&preis,&c);
  if(test!=1||c!='\0'||preis<=0)
    return 0;


  //Schauen ob das Buch schon existiert
  buch_t *buchptr=sucheBuch(buecher,buchAnzahl,isbn,0);
  if(buchptr){
    if(!strcmp(buchptr->titel,titel)){
      buchptr->bestand += bestand;
      buchptr->preis = preis;
    }
    free(titel);
    return 0;
  }
  else if(sucheBuch(buecher,buchAnzahl,titel,1)){
    free(titel);
    return 0;
  }

  strcpy(buecher[buchAnzahl].buchNummer,isbn);
  buecher[buchAnzahl].titel=calloc(strlen(titel)+1,sizeof(char));
  strcpy(buecher[buchAnzahl].titel,titel);
  buecher[buchAnzahl].bestand = bestand;
  buecher[buchAnzahl].preis = preis;
  free(titel);
  return 1;
}


buch_t *sucheBuch(buch_t *buecher,int buchAnzahl,char *suchText,int nummerOderTitel){

  //nummerOderTitel: 0 ist Nummer, 1 ist Titel

  int i;

  //Nummer
  if(!nummerOderTitel){
    for(i=0;i<buchAnzahl;i++){
      if(strstr(buecher[i].buchNummer,suchText))
        return &buecher[i];
    }
  }

  //Titel
  else {
    for(i=0;i<buchAnzahl;i++){
      if(strstr(buecher[i].titel,suchText))
        return &buecher[i];
    }
  }

  return NULL;
}

char menu(){

  char ein;
  char moeglich[] = "asfhetdbwoxz";

  do{
  printHeader();
  printf(">a: Buecher ausgeben\n");
  printf(">s: Buecher sortiert ausgeben\n");
  printf(">f: Buecher finden\n");
  printf("\n>h: Buch hinzufuegen\n");
  printf(">e: Buch entnehmen\n");
  printf(">t: Buchtitel aendern\n");
  printf(">d: Buch loeschen\n");
  printf("\n>b: Bestellliste ausgeben\n");
  printf(">w: Lagerwert ausgeben\n");

  printf("\n>o: Weitere Datei oeffnen und einlesen\n");
  printf(">x: Buecherliste exportieren (speichern)\n");
  printf(">z: Programm Beenden\n");
  printf("\nIhre Eingabe --> ");
  ein = getch();
}while(!strchr(moeglich,ein));

    return ein;
}

void printHeader(){

  system("cls");

  time_t t = time(NULL);
  struct tm tm = *localtime(&t);

printf("   _      _  _                       _        \n"
       "  | |    (_)| |                     (_)       \n"
       "  | |     _ | |__   _ __  ___  _ __  _   __ _ \n"
       "  | |    | || '_ \\ | '__|/ _ \\| '__|| | / _` |\n"
       "  | |____| || |_) || |  |  __/| |   | || (_| |\n"
       "  \\_____/|_||_.__/ |_|   \\___||_|   |_| \\__,_|\n\n");

  printf("\t\t    %02d:%02d",tm.tm_hour,tm.tm_min);
  printf("\n\t\t %02d. %02d. %d\n\n\n",tm.tm_mday,tm.tm_mon+1,tm.tm_year+1900);

}


void listeBuchbestand(buch_t *buecher,int anz){

  int maxString = getMaxTitel(buecher, anz);
  int i,strich=3+10+maxString+4+6+16+5;

  printHeader();

  printf("| Nr. |    ISBN    | %*s | Anzahl |   Preis   |\n",maxString,"Buchtitel");

  for(i=0;i<strich;i++)
    putchar('_');
  putchar('\n');

  for(i=0;i<anz;i++)
    printf("| %3d | %10s | %*s |  %4d  | %6.2lf EUR|\n",i+1,buecher[i].buchNummer,maxString,buecher[i].titel,buecher[i].bestand,buecher[i].preis);

  for(i=0;i<strich;i++)
    putchar('-');

  printf("\n\nTaste druecken...");
  getch();
}

int getMaxTitel(buch_t *buecher,int anz){

  int max=0,i;

  for(i=0;i<anz;i++){
    if(strlen(buecher[i].titel)>max)
      max = strlen(buecher[i].titel);
  }

  return max;
}


void toCSVText(buch_t *buch,char *csvText){

  sprintf(csvText,"%s;%s;%d;%lg",buch->buchNummer,buch->titel,buch->bestand,buch->preis);

}

int speichereBuecher(char *pfad,buch_t *buecher,int anzahl){

  printHeader();

  FILE *fp=fopen(pfad,"r");
  char mode[2]="w"; //Standardmaessig schreiben
  char moeglich[]="nuaz";
  char ein;
  char line[500];
  int i;

  if(fp){
    fclose(fp);

    do{
    printHeader();
    printf("Die angegebene Datei existiert bereits! Was soll getan werden?\n"
           ">n: Neuen Dateinamen eingeben\n"
           ">u: Datei ueberschreiben\n"
           ">a: An Datei anhaengen\n"
           ">z: Zurueck\n\n"
           "Ihre Eingabe --> ");
           ein=getch();
    }while(!strchr(moeglich,ein));

    switch(ein) {
      case 'n':
        printHeader();
        printf("Geben Sie einen neuen Dateinamen ein --> ");
        char pfadNeu[100];
        scanf("%s",pfadNeu);
        return speichereBuecher(pfadNeu,buecher,anzahl);
        break;

      case 'u': mode[0] = 'w'; break;

      case 'a': mode[0] = 'a'; break;

      case 'z': return 0; break;

      default:  return 0; break;  //Sollte nicht nötig sein wegen strchr
    }

  }

  fp = fopen(pfad,mode);
  if(!fp){
    printHeader();
    printf("Fehler! Die Datei konnte nicht geoeffnet/erstellt werden!\nTaste druecken...");
    getch();
    return 0;
  }

  fprintf(fp,"Buchnummer;Titel;Bestand;Preis\n");
  for(i=0;i<anzahl;i++){
    toCSVText(&buecher[i],line);
    fprintf(fp,"%s\n",line);
  }

  fclose(fp);
  return 1;
}


int buchZugang(buch_t *buecher, int buchAnzahl){

  printHeader();

  char ein[500] = "";
  char buf[125] = "";

  printf("Geben Sie die ISBN ein --> ");
  gets(buf);
  strcat(ein,buf);
  strcat(ein,";");

  printf("Geben Sie den Titel ein --> ");
  gets(buf);
  strcat(ein,buf);
  strcat(ein,";");

  printf("Geben Sie die Stueckzahl ein --> ");
  gets(buf);
  strcat(ein,buf);
  strcat(ein,";");

  printf("Geben Sie den Preis ein --> ");
  gets(buf);
  strcat(ein,buf);

  if(erstelleNeuesBuch(ein,buecher,buchAnzahl))
    return 1;
  else{
    printf("\nDie Eingaben sind fehlerhaft oder nicht gueltig!\nTaste druecken...");
    getch();
    return 0;
  }

}


void listeBuchbestandGeordnet(buch_t *buecher,int buchAnzahl,int ordnungNach){

  buch_t *geordnet = calloc(buchAnzahl,sizeof(buch_t));
  memcpy(geordnet,buecher,buchAnzahl*sizeof(buch_t));
  int (*cmp) (const void* v1,const void* v2);

  switch(ordnungNach) {

    case 1: cmp = &isbnCmp; break;
    case 2: cmp = &titelCmp; break;
    case 3: cmp = &bestandCmp; break;
    case 4: cmp = &preisCmp; break;
    case 5: cmp = &isbnDCmp; break;
    case 6: cmp = &titelDCmp; break;
    case 7: cmp = &bestandDCmp; break;
    case 8: cmp = &preisDCmp; break;
    default: cmp = &isbnCmp; break;

  }

  qsort(geordnet,buchAnzahl,sizeof(buch_t),cmp);

  listeBuchbestand(geordnet,buchAnzahl);
  free(geordnet);
}

int getSortOrder(){

  char ein;
  int order=0;

  do{
    printHeader();
    printf("Nach welchem Kriterium soll sortiert werden?\n");
    printf(">i: ISBN\n");
    printf(">t: Titel\n");
    printf(">b: Bestand\n");
    printf(">p: Preis\n");
    printf("\nIhre Wahl -->");
    ein = getch();
  }while(!strchr("itbp",ein));

  switch(ein) {
    case 'i': order=1; break;
    case 't': order=2; break;
    case 'b': order=3; break;
    case 'p': order=4; break;

  }
  do{
    printHeader();
    printf("Aufsteigend oder absteigend sortieren?\n");
    printf(">a: Aufsteigend\n");
    printf(">d: Absteigend\n");
    printf("\nIhre Wahl -->");
    ein = getch();
  }while(!strchr("ad",ein));

  if(ein=='d')
  order+=4;

  return order;
}

int buchEntnahme(buch_t *buecher,int anzahl){

  char suchstr[100];
  char ein;
  buch_t *buch=NULL;
  int nummerOderTitel,maxString,test,entnahme;

  do{
  printHeader();
  printf("Nach ISBN oder Titel entnehmen?\n");
  printf(">i: Nach ISBN entnehmen\n");
  printf(">t: Nach Titel entnehmen\n");
  printf(">z: Zurueck\n");
  printf("\nIhre Wahl -->");
  ein = getch();
  }while(!strchr("itz",ein));

  switch(ein){
    case 'i': nummerOderTitel=0; break;
    case 't': nummerOderTitel=1; break;
    case 'z': //Fallthough
    default: return 0;
  }

  printHeader();
  printf("Geben Sie den Suchstring ein --> ");
  gets(suchstr);

  buch = sucheBuch(buecher,anzahl,suchstr,nummerOderTitel);

  if(!buch){
    printHeader();
    printf("Buch konnte nicht gefunden werden!\nTaste druecken...");
    getch();
    return 0;
  }

  maxString=getMaxTitel(buch,1);
  do{
    printHeader();
    printf("    ISBN    |  %*s  | Anzahl |  Preis|\n",maxString,"Titel");
    printf(" %10s |  %*s  |  %4d  | %6.2lf|\n",buch->buchNummer,maxString-2,buch->titel,buch->bestand,buch->preis);
    printf("\n\nRichtiges Buch [J/n]");
    ein = getch();
  }while(!strchr("JjNn",ein));
  if(tolower(ein)=='n')
    return 0;

  printHeader();
  do{
    printf("Wieviel soll entnommen werden (%d verfuegbar)?\nIhre Eingabe --> ",buch->bestand);
    fflush(stdin);
    test = scanf("%d%c",&entnahme,&ein);
    if(test!=2||ein!='\n'||entnahme<=0){
      printHeader();
      printf("Falsche Eingabe!\n");
    }
  }while(test!=2||ein!='\n'||entnahme<=0);

  if(buch->bestand-entnahme<0){
    printHeader();
    printf("Fehler! Zu viele Buecher entnommen.\nTaste druecken");
    getch();
    return 0;
  }

  else if(buch->bestand-entnahme==0){
    do{
    printHeader();
    buch->bestand -= entnahme;
    printf("Es sind jetzt 0 Buecher vorhanden, soll das Buch entfernt werden [J/n]");
    ein = getch();
    }while(!strchr("JjNn",ein));

    if(tolower(ein)=='n')
      return 0;

    return buchEntfernen(buecher,anzahl,buch->buchNummer);
  }
  else {
    buch->bestand -= entnahme;
  }
  return 0;
}


int buchEntfernen(buch_t *buecher,int buchAnzahl,char* buchnummer){
  int pos=0;
  buch_t *buchptr=NULL;
  char ein;

  buchptr = sucheBuch(buecher,buchAnzahl,buchnummer,0);

  if(!buchptr){
    printHeader();
    printf("Fehler! ISBN konnte nicht gefunden werden.\nTaste druecken...");
    getch();
    return 0;
  }

  do{
    printHeader();
    printf("%s: %s\n",buchptr->buchNummer,buchptr->titel);
    printf("Soll dieses Buch wirklich geloescht werden [J/n]");
    ein = getch();
  }while(!strchr("JjNn",ein));

  if(tolower(ein)=='n')
    return 0;

  pos=buchptr-buecher;
  free(buchptr->titel);
  memcpy(buecher+pos,buecher+pos+1,(buchAnzahl-pos)*sizeof(buch_t));
  return 1;
}

buch_t *getBuchDialog(buch_t *buecher, int anzahl){
  char suchstr[100];
  char ein;                     //FUnktion zum buch bestimmen weil main zu unordentlich
  buch_t *buch=NULL;
  int nummerOderTitel,maxString;

  do{
  printHeader();
  printf("Nach ISBN oder Titel entfernen?\n");
  printf(">i: Nach ISBN\n");
  printf(">t: Nach Titel entfernen\n");
  printf(">z: Zurueck\n");
  printf("\nIhre Wahl -->");
  ein = getch();
  }while(!strchr("itz",ein));

  switch(ein){
    case 'i': nummerOderTitel=0; break;
    case 't': nummerOderTitel=1; break;
    case 'z': //Fallthough
    default: return NULL;
  }

  printHeader();
  printf("Geben Sie den Suchstring ein --> ");
  gets(suchstr);

  buch = sucheBuch(buecher,anzahl,suchstr,nummerOderTitel);

  if(!buch){
    printHeader();
    printf("Buch konnte nicht gefunden werden!\nTaste druecken...");
    getch();
    return NULL;
  }

  maxString=getMaxTitel(buch,1);
  do{
    printHeader();
    printf("    ISBN    |  %*s  | Anzahl |  Preis|\n",maxString,"Titel");
    printf(" %10s |  %*s  |  %4d  | %6.2lf|\n",buch->buchNummer,maxString-2,buch->titel,buch->bestand,buch->preis);
    printf("\n\nRichtiges Buch [J/n]");
    ein = getch();
  }while(!strchr("JjNn",ein));
  if(tolower(ein)=='n')
    return NULL;
  return buch;
}


void aendereTitel(buch_t *buchptr){

  char titelNeu[200];
  char ein;

  printHeader();
  printf("Geben Sie einen neuen Buchtitel ein (alter Titel: \"%s\"): ",buchptr->titel);
  gets(titelNeu);

  do{
    printHeader();
    printf("Wollen Sie den Titel von \"%s\" auf \"%s\" aendern [J/n]",buchptr->titel,titelNeu);
    ein=getch();
  }while(!strchr("JjNn",ein));
  if(tolower(ein)=='n')
    return;
  buchptr->titel = realloc(buchptr->titel,strlen(titelNeu)+1);
  strcpy(buchptr->titel,titelNeu);
}

void buecherFinden(buch_t *buecher,int buchAnzahl){
  char ein;

  do{
    printHeader();
    printf(">s: Buecher suchen\n");
    printf(">f: Buecher filtern\n");
    printf(">z: Zurueck");
    ein=getch();
  }while(!strchr("sfz",ein));

  switch(ein) {
    case 's': buecherSuchen(buecher,buchAnzahl); break;
    case 'f': buecherFiltern(buecher,buchAnzahl); break;
    case 'z': //Fallthrough
    default: break;
  }
}

void buecherSuchen(buch_t *buecher,int buchAnzahl){
  int ein,nummerOderTitel=0;
  char suchstring[100];
  buch_t *buchptr;

  do{
    printHeader();
    printf(">i: Nach ISBN suchen\n");
    printf(">t: Nach Titel suchen\n");
    printf(">z: Zurueck\n");
    ein = getch();
  }while(!strchr("itz",ein));

  switch(ein) {
    case 'i': nummerOderTitel=0; break;
    case 't': nummerOderTitel=1; break;
    case 'z':
    default: return; break;
  }

  printHeader();
  printf("Suchstring eingeben --> ");
  gets(suchstring);

  buchptr = sucheBuch(buecher,buchAnzahl,suchstring,nummerOderTitel);

  if(!buchptr){
    printHeader();
    printf("Suchstring wurde nicht gefunden.\nTaste druecken...");
    getch();
    return;
  }

  int maxString = strlen(buchptr->titel);
  printHeader();
  printf("    ISBN    |  %*s  | Anzahl |  Preis|\n",maxString,"Titel");
  printf(" %10s |  %*s  |  %4d  | %6.2lf|\n",buchptr->buchNummer,maxString-2,buchptr->titel,buchptr->bestand,buchptr->preis);

  printf("\nTaste druecken...");
  getch();
}

void buecherFiltern(buch_t *buecher,int buchAnzahl){
  int fein,ein,test,mehrOderWeniger;
  char suchstring[100],c;
  double preis;

  do{
    printHeader();
    printf(">i: Nach ISBN filtern\n");
    printf(">t: Nach Titel filtern\n");
    printf(">p: Nach Preis filtern\n");
    printf(">z: Zurueck\n");
    fein = getch();
  }while(!strchr("itpz",fein));

  if(fein!='p'){
    printHeader();
    printf("Suchstring eingeben --> ");
    gets(suchstring);
  }
  else if(fein=='p'){
    printHeader();
    printf("Preis eingeben --> ");
    fflush(stdin);
    test = scanf("%lf%c",&preis,&c);
    if(test!=2||c!='\n'||preis<0){
      printHeader();
      printf("Fehler! Falsche Eingabe!\nTaste druecken...");
      getch();
      return;
    }

    do{
      printHeader();
      printf(">m: Buecher ueber %lg EUR\n",preis);
      printf(">w: Buecher unter %lg EUR\n",preis);
      printf(">z: Zurueck\n");
      ein = getch();
    }while(!strchr("mwz",ein));

      switch(ein){
        case 'm': mehrOderWeniger=0; break;
        case 'w': mehrOderWeniger=1; break;
        case 'z':
        default: return; break;
      }
  }
  printHeader();
  switch(fein) {
    case 'i': nummerFilter(buecher,buchAnzahl,suchstring); break;
    case 't': titelFilter(buecher,buchAnzahl,suchstring); break;
    case 'p': preisFilter(buecher,buchAnzahl,preis,mehrOderWeniger); break;
    case 'z':
    default: return; break;
  }
  printf("\n\nTaste druecken...");
  getch();
}


double lagerWert(buch_t *buecher,int buchAnzahl){
  double sum = 0;
  int i;

  for(i=0;i<buchAnzahl;i++)
    sum += buecher[i].bestand * buecher[i].preis;

  return sum;
}


void titelFilter(buch_t *buecher,int buchAnzahl,char *suchstring){
  int i,maxString=getMaxTitel(buecher,buchAnzahl);
  int strich=3+10+maxString+4+6+16+5;

  printf("| Nr. |    ISBN    | %*s | Anzahl |   Preis   |\n",maxString,"Buchtitel");

  for(i=0;i<strich;i++)
    putchar('_');
  putchar('\n');

  for(i=0;i<buchAnzahl;i++){
    if(strstr(buecher[i].titel,suchstring))
      printf("| %3d | %10s | %*s |  %4d  | %6.2lf EUR|\n",i+1,buecher[i].buchNummer,maxString,buecher[i].titel,buecher[i].bestand,buecher[i].preis);

  }
  for(i=0;i<strich;i++)
  putchar('-');
}

void nummerFilter(buch_t *buecher,int buchAnzahl,char *suchstring){
  int i,maxString=getMaxTitel(buecher,buchAnzahl);
  int strich=3+10+maxString+4+6+16+5;

  printf("| Nr. |    ISBN    | %*s | Anzahl |   Preis   |\n",maxString,"Buchtitel");

  for(i=0;i<strich;i++)
    putchar('_');
  putchar('\n');

  for(i=0;i<buchAnzahl;i++){
    if(strstr(buecher[i].buchNummer,suchstring))
      printf("| %3d | %10s | %*s |  %4d  | %6.2lf EUR|\n",i+1,buecher[i].buchNummer,maxString,buecher[i].titel,buecher[i].bestand,buecher[i].preis);

  }
  for(i=0;i<strich;i++)
  putchar('-');
}

void preisFilter(buch_t *buecher,int buchAnzahl,double preis,int mehrOderWeniger){
  int i,maxString=getMaxTitel(buecher,buchAnzahl);
  int strich=3+10+maxString+4+6+16+5;

  printf("| Nr. |    ISBN    | %*s | Anzahl |   Preis   |\n",maxString,"Buchtitel");

  for(i=0;i<strich;i++)
    putchar('_');
  putchar('\n');

  for(i=0;i<buchAnzahl;i++){
    switch(mehrOderWeniger) {
      case 0:
        if(buecher[i].preis>=preis)
          printf("| %3d | %10s | %*s |  %4d  | %6.2lf EUR|\n",i+1,buecher[i].buchNummer,maxString,buecher[i].titel,buecher[i].bestand,buecher[i].preis);
        break;

      case 1:
        if(buecher[i].preis<=preis)
          printf("| %3d | %10s | %*s |  %4d  | %6.2lf EUR|\n",i+1,buecher[i].buchNummer,maxString,buecher[i].titel,buecher[i].bestand,buecher[i].preis);
        break;

    }
  }
  for(i=0;i<strich;i++)
  putchar('-');
}

void bestellListe(buch_t *buecher,int buchAnzahl){

  printHeader();

  int i,maxString=getMaxTitel(buecher,buchAnzahl);
  int strich=3+10+maxString+4+6+16+5;

  printf("Zu bestellende Buecher:\n\n");

  printf("| Nr. |    ISBN    | %*s | Anzahl |   Preis   |\n",maxString,"Buchtitel");

  for(i=0;i<strich;i++)
    putchar('_');
  putchar('\n');

  for(i=0;i<buchAnzahl;i++){
    if(buecher[i].bestand<5)
      printf("| %3d | %10s | %*s |  %4d  | %6.2lf EUR|\n",i+1,buecher[i].buchNummer,maxString,buecher[i].titel,buecher[i].bestand,buecher[i].preis);
  }

  for(i=0;i<strich;i++)
  putchar('-');
  printf("\n\nTaste druecken...");
  getch();
}


//CMP Funks

int isbnCmp(const void *v1,const void *v2){

  buch_t *b1 = (buch_t*) v1;
  buch_t *b2 = (buch_t*) v2;

  return strcmp(b1->buchNummer,b2->buchNummer);
}
int titelCmp(const void *v1,const void *v2){

  buch_t *b1 = (buch_t*) v1;
  buch_t *b2 = (buch_t*) v2;

  return strcmp(b1->titel,b2->titel);
}
int bestandCmp(const void *v1,const void *v2){

  buch_t *b1 = (buch_t*) v1;
  buch_t *b2 = (buch_t*) v2;

  return bigger(b1->bestand,b2->bestand);
}
int preisCmp(const void *v1,const void *v2){

  buch_t *b1 = (buch_t*) v1;
  buch_t *b2 = (buch_t*) v2;

  return bigger(b1->preis,b2->preis);
}
int isbnDCmp(const void *v1,const void *v2){

  buch_t *b1 = (buch_t*) v1;
  buch_t *b2 = (buch_t*) v2;

  return strcmp(b2->buchNummer,b1->buchNummer);
}
int titelDCmp(const void *v1,const void *v2){

  buch_t *b1 = (buch_t*) v1;
  buch_t *b2 = (buch_t*) v2;

  return strcmp(b2->titel,b1->titel);
}
int bestandDCmp(const void *v1,const void *v2){

  buch_t *b1 = (buch_t*) v1;
  buch_t *b2 = (buch_t*) v2;

  return lesser(b1->bestand,b2->bestand);
}
int preisDCmp(const void *v1,const void *v2){

  buch_t *b1 = (buch_t*) v1;
  buch_t *b2 = (buch_t*) v2;

  return lesser(b1->preis,b2->preis);
}
